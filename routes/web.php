<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 2/1/2018
 * Time: 1:03 PM
 */
Route::group(['middleeware' => ['web']], function(){

Route::get('/',function (){
    return view ('foodprice');
});

Route::get('/foodprice',function (){
    return view ('foodprice');
});

Route::get('/test', ['as'=>'testing',function(){
    return view('test2');
}]);
Route::get('redirect',function(){
    return redirect()->route('testing');
});

Route::get('/register',function (){
    return view ('register');
});
Route::get('register','RegprocessController@insertform');
Route::POST('register','RegprocessController@insert');

Route::get('/loginpro',function (){
    return view ('loginpro');
});

Route::get('loginpro','LoginController@index');
Route::post('loginpro','LoginController@check');

Route::get('/adminlogin1',function (){
    return view ('adminlogin1');
});

Route::get('adminlogin1','AdminloginController@index');
Route::post('adminlogin1','AdminloginController@adminvalid');

Route::get('/admincustomer',function (){
    return view ('admincustomer');
});

Route::get('admincustomer','AdmincustomerController@index');

Route::get('/adminproduct',function (){
    return view ('adminproduct');
});

Route::get('adminproduct','AdminproductController@index');

Route::get('/adminview',function (){
    return view ('adminview');
});

Route::get('/place','AdminviewsinglecustController@customercheck');
    Route::get('/place','AdminloginController@check11');

Route::get('/product',function (){
    return view ('product');
});
Route::get('product','ProductController@index');
Route::post('product','ProductController@productcheck');
    Route::get('prod','LoginController@check1');

Route::get('/product','ProductController@productcheck');

Route::get('/scrapping',function (){
    return view ('scrapping');
});

Route::get('scrapping','ScrappingController@index');
Route::get('scrap','ScrappingController@scrap');

Route::get('/logout',['as'=>'logout','user'=>'LogoutCountroller@logout']);
});