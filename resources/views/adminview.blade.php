<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Admin View Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        table-border td, .table-border td,th{  border:5px solid #adadad;  }
        .navbar {  margin-bottom: 0;  border-radius: 0;  }
    </style>
</head>
<nav class="navbar navbar-inverse">
    <a class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">View The Detail About The Customer..</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="scrap"><span class="glyphicon glyphicon-log-in"></span>Scrapping</a></li>
            <li><a href="admincustomer"><span class="glyphicon glyphicon-log-in"></span>Customer</a></li>
            <li><a href="adminproduct"><span class="glyphicon glyphicon-log-in"></span>Product Report</a></li>
            <li><a href="adminlogin1"><span class="glyphicon glyphicon-log-in"></span>Logout</a></li>
        </ul>
    </div>
</nav>
<body style="background-color: #F0E68C">
<center> <h3><font face="Agency FB" size="">Select Our Viewing Customer Place</font></h3> </center>
<hr>
<font face="Algerian FB">
    <div class="col-md-12" id="b1" align="center">
        <table bgcolor="" class='table table-striped'>
            <tr>
                <td>Welcome:
                    <?php echo $users['users']->adminname; ?></td>
            </tr>
        </table>
    </div>
</font>
<br>
<hr>

<form action="/place" method="get">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <input type="hidden" name="email" value="<?php echo $users['users']->email; ?>"/>
    <div align="center">
        <table class="table thumbnail" align="center" style="width: 20%" height="20%">
            <tr>
                <td><font face="Algerian FB">Place:</font></td>
                <td><select name="place" class="form-control" value="" required="" >
                        <option value="">Select the place</option>
                        <option value="Ariyalur">Ariyalur</option>
                        <option value="Chennai">Chennai</option>
                        <option value="Coimbatore">Coimbatore</option>
                        <option value="Cuddalore">Cuddalore</option>
                        <option value="Dharmapuri">Dharmapuri</option>
                        <option value="Dindigul">Dindigul</option>
                        <option value="Erode">Erode</option>
                        <option value="Kancheepuram">Kancheepuram</option>
                        <option value="Karur">Karur</option>
                        <option value="Krishnagiri">Krishnagiri</option>
                        <option value="Madurai">Madurai</option>
                        <option value="Nagapattinam">Nagapattinam</option>
                        <option value="Nagercoil">Nagercoil</option>
                        <option value="Namakkal">Namakkal</option>
                        <option value="Perambalur">Perambalur</option>
                        <option value="Pudukkottai">Pudukkottai</option>
                        <option value="Ramanathapuram">Ramanathapuram</option>
                        <option value="Salem">Salem</option>
                        <option value="Sivaganga">Sivaganga</option>
                        <option value="Thanjavur">Thanjavur</option>
                        <option value="Thenilgiris">Thenilgiris</option>
                        <option value="Theni">Theni</option>
                        <option value="Thiruchirappali">Thiruchirappali</option>
                        <option value="Thiruvarur">Thiruvarur</option>
                        <option value="Thiruvellore">Thiruvellore</option>
                        <option value="Tuticorin">Tuticorin</option>
                        <option value="Vellore">Vellore</option>
                        <option value="Vilupuram">Vilupuram</option>
                        <option value="Virudhunagar">Virudhunagar</option>
                    </select></td>
                <span style="color: red"></span>
            </tr>
            <tr>
                <td><font face="Algerian FB"><button name="view" class="btn btn-success" value="view">View</button></font></td>
            </tr>
        </table>
    </div>
</form>
<br>
@if (Session::has('message'))
    <center>
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    </center>
@endif
<hr>
    <font face="Algerian FB">
<form name="/adminview" action="" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="col-md-12" id="b1" align="center">
        <table class="table table-striped" align="center" style="">
            <tr>
                <th>Name</th>
                <th>Place</th>
                <th>Phone</th>
                <th>E-mail</th>
            </tr>
            @if(count($users['product_details']) && $users['product_details'] )
                @foreach ($users['product_details'] as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->place }}</td>
                        <td>{{ $product->phone}}</td>
                        <td>{{ $product->email}}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</form>
</font>
</body>
</html>