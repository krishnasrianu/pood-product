<html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Register Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        .navbar
        {  margin-bottom: 0;
            border-radius: 0;
        }
    </style>
</head>
<nav class="navbar navbar-inverse">
    <a  class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">User Register Page..</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
        </ul>
    </div>
</nav>
<body style="background-color: #F0E68C">
<br>
<center> <h4><font face="Algerian FB">Enter The Valid Address..</font></h4> </center>
<hr>
<form action = "/register" method = "post">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
    <table align="center" class="table thumbnail" align="center" style="width: 30%" height="40%" >
        <tr>
            <td><font face="Algerian FB">Name:</font></td>
            <td><input type="text" name="name" class="form-control" required pattern ="[a-z.A-Z.].{3,15}$" placeholder="Enter name" size="30"></td>
        </tr>
        <tr>
            <td><font face="Algerian FB">Place:</font></td>
            <td><select name="place" class="form-control" required >
                    <option value="">Select the place</option>
                    <option value="Ariyalur">Ariyalur</option>
                    <option value="Chennai">Chennai</option>
                    <option value="Coimbatore">Coimbatore</option>
                    <option value="Cuddalore">Cuddalore</option>
                    <option value="Dharmapuri">Dharmapuri</option>
                    <option value="Dindigul">Dindigul</option>
                    <option value="Erode">Erode</option>
                    <option value="Kancheepuram">Kancheepuram</option>
                    <option value="Karur">Karur</option>
                    <option value="Krishnagiri">Krishnagiri</option>
                    <option value="Madurai">Madurai</option>
                    <option value="Nagapattinam">Nagapattinam</option>
                    <option value="Nagercoil">Nagercoil</option>
                    <option value="Namakkal">Namakkal</option>
                    <option value="Perambalur">Perambalur</option>
                    <option value="Pudukkottai">Pudukkottai</option>
                    <option value="Ramanathapuram">Ramanathapuram</option>
                    <option value="Salem">Salem</option>
                    <option value="Sivaganga">Sivaganga</option>
                    <option value="Thanjavur">Thanjavur</option>
                    <option value="Thenilgiris">Thenilgiris</option>
                    <option value="Theni">Theni</option>
                    <option value="Thiruchirappali">Thiruchirappali</option>
                    <option value="Thiruvarur">Thiruvarur</option>
                    <option value="Thiruvellore">Thiruvellore</option>
                    <option value="Tuticorin">Tuticorin</option>
                    <option value="Vellore">Vellore</option>
                    <option value="Vilupuram">Vilupuram</option>
                    <option value="Virudhunagar">Virudhunagar</option>
                </select></td>
        </tr>
        <tr>
            <td><font face="Algerian FB">Phone:</font></td>
            <td><input type="text" name="phone" class="form-control" required pattern="[0-9]{10}" placeholder="Enter Phone" size="30"/></td>
        </tr>
        <tr>
            <td><font face="Algerian FB">Email:</font></td>
            <td><input type="text" name="email" class="form-control" required pattern="[a-z0-9._%+=]+@[a-z.-]+\.[a-z]{2,4}$" placeholder="Enter Email" size="30"/></td>
        </tr>
        <tr>
            <td><font face="Algerian FB">Password:</font></td>
            <td><input type="password" name="password" class="form-control" required pattern ="[a-zA-Z0-9!@#$%^&*]{5,8}$" placeholder="Enter Password" size="30"/></td>
        </tr>
    </table>
    </center>
    <center><font face="Algerian FB">
        <button type="submit" name="reg" class="btn btn-success" value="Register"><i class="fa fa-check" style="font-size:28px;color:white"></i> Register</button>
        <button type="reset"  name="reset" class="btn btn-danger" value="Cancel"><i class="fa fa-times-circle" style="font-size:28px;color:black"></i> Cancel</button>
        <a href="loginpro"><font color="green">Back To Login.. </font></a>
        </font> </center>
    @if (Session::has('message'))
        <center>
            <div class="alert alert-info">{{ Session::get('message') }}</div></center>
    @endif
    <hr>
</form>
</body>
</html>