<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Admin View The Product Updates</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        table-border td, .table-border td,th{  border:5px solid #adadad;  }
        .navbar {  margin-bottom: 0;  border-radius: 0;  }
    </style>
</head>
<nav class="navbar navbar-inverse">
    <a  class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">View The Detail About The Product Updates..</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href=""><span class="glyphicon glyphicon-log-in"></span>Back..</a></li>
        </ul>
    </div>
</nav>
<hr>
<body style="background-color: #F0E68C">
<font face="Algerian FB">
<br>
<form name="/adminview" action="" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="col-md-12" id="b1" align="center">
        <table id="customers" class='table table-striped'>
            <tr>
                <th>Date</th>
                <th>Product Name</th>
                <th>Price</th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->date }}</td>
                    <td>{{ $user->productname }}</td>
                    <td>{{ $user->price }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</form>
</font>
</body>
</html>