<html>
<head>
    <title>Food Product Price Recommendations And Predictions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        .carousel-inner img {
            width: 150%;
            height: 200px;
            margin: auto;
        }

        /* Hide the carousel text when the screen is less than 600 pixels wide */
        @media (max-width: 600px) {
            .carousel-caption {
                display: none;
            }
        }

        table-border td, .table-border td, th {
            border: 2px solid #adadad;
        }
    </style>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="navbar-header">
        <a href="" class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">Food Product
            Price Recommendations And Predictions</a>
    </div>

    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="adminlogin1"><span class="glyphicon glyphicon-log-in"></span>Admin</a></li>
        </ul>
    </div>
    </div>
</nav>

<font face="Algerian FB">
    <hr>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="image/a1.PNG" alt="Los Angeles" style="width:100%;">
                            </div>

                            <div class="item">
                                <img src="image/a2.PNG" alt="Chicago" style="width:100%;">
                            </div>

                            <div class="item">
                                <img src="image/d3.PNG" alt="New york" style="width:100%;">
                            </div>
                            <div class="item">
                                <img src="image/i4.PNG" alt="New york" style="width:100%;">
                            </div>
                            <div class="item">
                                <img src="image/y6.PNG" alt="New york" style="width:100%;">
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <table border="2" align="center">
                        <tr bgcolor="#e6e6fa">
                            <th>SoNo</th>
                            <th>DistrictName</th>
                            <th>MarketName</th>
                            <th>Commodity</th>
                            <th>Variety</th>
                            <th>MinPrice</th>
                            <th>MaxPrice</th>
                            <th>ModelPrice</th>
                            <th>PriceDate</th>
                        </tr>
                        <tr bgcolor="">
                            <td>1</td>
                            <td>Salem</td>
                            <td>Salem</td>
                            <td>Turmeric</td>
                            <td>Finger</td>
                            <td>7600</td>
                            <td>9100</td>
                            <td>8500</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>2</td>
                            <td>Salem</td>
                            <td>Salem</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>3</td>
                            <td>vellore</td>
                            <td>vellore</td>
                            <td>Turmeric</td>
                            <td>Finger</td>
                            <td>7600</td>
                            <td>9100</td>
                            <td>8500</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>4</td>
                            <td>vellore</td>
                            <td>vellore</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>5</td>
                            <td>Erode</td>
                            <td>Erode</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>6</td>
                            <td>Dhirumaburi</td>
                            <td>Dhirumaburi</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>7</td>
                            <td>Dhirumaburi</td>
                            <td>Dhirumaburi</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                        <tr bgcolor="">
                            <td>8</td>
                            <td>Erode</td>
                            <td>Erode</td>
                            <td>Turmeric</td>
                            <td>Bulb</td>
                            <td>7050</td>
                            <td>8050</td>
                            <td>7700</td>
                            <td>20 FEB 2018</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="some_class">
                    <ul>
                        <ol><font face="Agency FB" size="10"><u>Available Product:</u></font></ol>
                        <ol>Paddy, Sugarcane, Banyan tree, Wheat, Turmeric, Brinjal, Carrot, Beetroot</ol>
                        <ol>Turmeric is a very important spice in India and finds extensive use in Indian
                            cuisine.Besides
                            food
                            flavouring,
                            Turmeric is a powerful medicine that has long been used in the Chinese and Indian systems of
                            medicine as an
                            anti-inflammatory agent to treat a wide variety of conditions.
                        </ol>
                        <ol>Turmeric is believed to ward off alzheimers disease, prevent cancer, reduce the risk of
                            heart
                            attacks, combat
                            inflammatory diseases and fight cold and flus.
                        </ol>
                        <br>
                        <br>
                        <br>
                        <center>
                            <h4><font class="form-control" face="Algerian FB" align="center"><a href="loginpro">Click
                                        Hear to See More..</a></font>
                            </h4>
                        </center>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <footer class="container-fluid text-center" solid #adadad>
            <nav class="navbar navbar-default navbar-fixed-bottom" style="background-color:#004d1a;">
                <div style="background-color:#004d1a;">
                    <center><label style="color: #fff;font-family: cambria;font-size: 15px;">Copyright © 2018-2024 Food
                            Product Price Predictions And Recommendations by GML Tech</label></center>
                </div>
            </nav>
        </footer>
    </div>
</font>

</body>
</html>