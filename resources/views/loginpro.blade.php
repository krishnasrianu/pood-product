<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Login Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        .navbar {  margin-bottom: 0;  border-radius: 0;  }
    </style>
</head>
<nav class="navbar navbar-inverse">
    <a  class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">User Login Page..</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="register"><span class="glyphicon glyphicon-log-in"></span> Register</a></li>
        </ul>
    </div>
</nav>
<body style="background-color: #F0E68C">
<br>
<center> <h4><font face="Algerian FB">Existing User Enter The Email Id and Password</font></h4> </center>
<hr>
<form name="/loginpro" action="" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div align="center">
        <table class="table thumbnail" align="center" style="width: 30%" height="20%">
            <tr>
                <td><font face="Algerian FB">Email:</font></td>
                <td><input type="text" name="email" class="form-control" placeholder="email-id"  required pattern="[a-z0-9._%+=]+@[a-z.-]+\.[a-z]{2,4}$" size="30"  /></td>
                <span style="color: red"></span>
            </tr>
            <tr>
                <td><font face="Algerian FB">Pass Word:</font></td>
                <td><input type="password" name="password" class="form-control" placeholder="password"  required pattern ="[a-zA-Z0-9!@#$%^&*]{5,8}$" size="30" /></td>
                <span style="color: red"></span>
            </tr>
        </table>
    </div>
    <center><font face="Algerian FB">
        <button name="option" class="btn btn-success" value="login"><i class="fa fa-check"
                                                                       style="font-size:28px;color:white"></i><font face="Agency FB" color=""> login</font></button>
        <button type="reset" class="btn btn-danger" name="option" value="Clear"><i class="fa fa-times-circle" style="font-size:28px;color:black"></i><font face="Agency FB" color="">Clear</font></button>
        <a href="foodprice">Back To Home..</a>
        </font></center>
    <br>
    @if (Session::has('message'))
        <center>
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        </center>
    @endif
</form>
<hr>
<ul><ol><font face="Algerian FB" size="5" align="center">New User Register FIrst. Register Link Availabel in Top of The Page Right Side.</font></ol></ul>
</body>
</html>