<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>View Detail in Product</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
    <style>
        table-border td, .table-border td, th {
            border: 5px solid #adadad;
        }

        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }
    </style>
</head>

<nav class="navbar navbar-inverse">
    <a class="navbar-brand" style="font-family: 'Colonna MT';font-size: 40px; align:center;">View The Detail About The
        Product..</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="loginpro"><span class="glyphicon glyphicon-log-in"></span>Logout</a></li>
        </ul>
    </div>
</nav>

<body style="background-color: #F0E68C">
<center><h3><font face="Algerian FB">Enter The Valid Date..</font></h3></center>
<hr>
<font face="Algerian FB">
    <div class="col-md-12" id="b1" align="center">
        <table bgcolor="" class='table table-striped'>
            <tr>
                <td>Welcome:
                    <?php echo $users['users']->name; ?></td>
            </tr>
        </table>
    </div>
</font>
<br>
<hr>
<form action="/prod" method="get">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <input type="hidden" name="email" value="<?php echo $users['users']->email; ?>"/>
    <div align="center">
        <table class="table thumbnail" align="center" style="width: 20%" height="20%">
            <tr>
                <td><font face="Algerian FB">Date:</font></td>
                <td><input type="text" name="date" class="form-control" placeholder="EX format: 01 JAN 2018" value=""
                           required pattern="[A-Za-z 0-9]{11}$" size="30"/></td>
                <span style="color: red"></span>
            </tr>
            <tr>
                <td><font face="Algerian FB">
                        <button name="submit" class="btn btn-success" value="view" >View</button>
                    </font></td>
            </tr>
        </table>
    </div>
</form>
<br>
@if (Session::has('message'))
    <center>
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    </center>
@endif
<hr>
<font face="Algerian FB">
    <form name="/product" action="" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="col-md-12" id="b1" align="center">
            <table bgcolor="" class='table table-striped'>
                <tr>
                    <th>DATE</th>
                    <th>PRODUCT NAME</th>
                    <th>PRICE</th>
                </tr>
                @if(count($users['product_details']) && $users['product_details'] )
                    @foreach ($users['product_details'] as $product)
                        <tr>
                            <td>{{ $product->date }}</td>
                            <td>{{ $product->productname }}</td>
                            <td>{{ $product->price }}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </form>
</font>
</body>
<hr>
</html>

