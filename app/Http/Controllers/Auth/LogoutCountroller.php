<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogoutCountroller extends Controller {
    public function dologout()
    {
        Auth::logout();
        Session::flush();
        return redirect('check2');
    }
}
//Session::flush();
//return redirect('/');