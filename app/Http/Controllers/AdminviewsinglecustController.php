<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminviewsinglecustController extends Controller
{
    public function index()
    {
        return view('adminview');
    }
    public function customercheck(Request $request)
    {
       if( DB::table('register')->where('place',$request->place)->get()){
           $users = DB::table('register')->where('place',$request->place)->get();
           return view('adminview')->with('users' ,$users);
       }else {
           return redirect()->back()->with('message', 'No Record');
       }

    }
}
