<?php
//namespace App\Http\Controllers;
//use Illuminate\Http\Request;
//use DB;
//use App\Http\Requests;
//use App\Http\Controllers\Controller;
//
//class AdminloginController extends Controller
//{
//    public function index()
//    {
//        return view('adminlogin1');
//    }
//    public function adminvalid(Request $request)
//    {
//        if( DB::table('loginadmin')->where('email',$request->email)->where('password',$request->password)->first())
//        {
//            $users=DB::table('register')->where('place',$request->place)->get();
//            return view('adminview')->with('users',$users);
//        }
//        else
//            {
//            return redirect()->back()->with('message','Incorrect Login');
//            }
//    }
//}
//
//



namespace App\Http\Controllers;
use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminloginController extends Controller
{
    public function index()
    {
        return view('adminlogin1');
    }

    public function adminvalid(Request $request)
    {
        if (DB::table('loginadmin')->where('email',$request->email)->where('password',$request->password)->first()) {
            $users = DB::table('loginadmin')->where('email', $request->email)->where('password', $request->password)->first();
            $data['users'] = $users;
            $data['product_details'] = '';
            return view('adminview')->with('users', $data);
        } else {
            return redirect()->back()->with('message', 'Login Incorrect');
        }
    }

    public function check11(Request $request)
    {
        $date = $request->place;
        $email = $request->email;

        if($date && $email){
            $product_details = DB::table('register')->where('place',$request->place)->get();

            $users = DB::table('loginadmin')->where('email', $email)->first();

            $data['product_details'] = $product_details;
            $data['users'] = $users;
            return view('adminview')->with('users', $data);
        }

    }

}
