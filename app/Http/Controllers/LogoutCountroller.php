<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogoutCountroller extends Controller {
    public function logout()
    {
        \Auth::logout(); // log the user out of our application
        return Redirect()-> route('loginpro'); // redirect the user to the login screen
    }
}