<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegprocessController extends Controller
{
    public function insertform()
    {
        return view('register');
    }
    public function insert(Request $request)
    {
        $name = $request->name;
        $place = $request->place;
        $phone = $request->phone;
        $email = $request->email;
        $password = $request->password;
        DB::insert('insert into register (name, place, phone,email,password) values(?,?,?,?,?)', [$name, $place, $phone, $email, $password]);
        return redirect()->back()->with('message','Registered Succesfully ...');
    }
}