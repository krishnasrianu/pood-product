<?php
namespace App\Http\Controllers;
use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index()
    {
        return view('loginpro');
    }

    public function check(Request $request)
    {
        if (DB::table('register')->where('email', $request->email)->where('password', $request->password)->first()) {
            $users = DB::table('register')->where('email', $request->email)->where('password', $request->password)->first();
            $data['users'] = $users;
            $data['product_details'] = '';
            return view('product')->with('users', $data);
        } else {
            return redirect()->back()->with('message', 'Login Incorrect');
        }
    }

    public function check1(Request $request)
    {
        $date = $request->date;
        $email = $request->email;

        if($date && $email){
            $product_details = DB::table('scraping')->where('date', $date)->get();

            $users = DB::table('register')->where('email', $email)->first();

            $data['product_details'] = $product_details;
            $data['users'] = $users;
            return view('product')->with('users', $data);
        }

    }

}
