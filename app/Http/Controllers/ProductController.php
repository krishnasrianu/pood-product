<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        return view('product');
    }

    public function productcheck(Request $request)
    {
       if(DB::table('scraping')->where('date',$request->date)->get()){
           $users = DB::table('scraping')->where('date',$request->date)->get();
           return view('product')->with('users' ,$users);
       } else {
           return redirect()->back()->with('message', 'No Record');
       }

    }
}
